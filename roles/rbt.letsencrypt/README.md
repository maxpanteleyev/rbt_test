# rbt.letsencrypt
=========

Test role to launch a docker contaner with simple app using automatically generated Let's Encrypt certificate

## Requirements
------------

Docker to run containers

# Role Variables
--------------

## Defaults
```
docker_volume_path | "/var/lib/docker/volumes" | Docker volume location
acme_account_key | "{{ acme_cert_storage_dir }}/account/{{ acme_account_name }}.key" | acme account short name
acme_key_size | 4096 | key bit length
acme_validity_min | 30 | The number of days the certificate must have left being valid.
acme_directory_validate | yes | Whether calls to the ACME directory will validate TLS certificates.
acme_default_challenge | dns-01 | Default ACME challenge to be performed.
acme_dns_wait | 60 | Timeout in seconds to wait for DNS propagation

Urls for acme directories:
acme_directory | "https://acme-staging-v02.api.letsencrypt.org/directory" | Staging URL for acme directory
acme_directory | "https://acme-v02.api.letsencrypt.org/directory"         | Production URL for acme directory
acme_ca_url | 'https://letsencrypt.org/certs/fakeleintermediatex1.pem'    | Staging URL for CA cert
acme_ca_url | 'https://letsencrypt.org/certs/isrgrootx1.pem'              | Production URL for CA cert
acme_ocsp_url | 'https://letsencrypt.org/certs/isrg-root-ocsp-x1.pem.txt' | URL for ocsp acme cert
```
## Variables
```
acme_cert_owner | username | Username to run tasks
acme_cert_storage_dir |  /home/{{ acme_cert_owner }}/letsencrypt_certs" | Directory on Ansible Controller to store created certs
acme_account_name | username | ACME account key file name
acme_account_email | test_domain@test_domain.com | ACME email name
docker_project_name | Dir name for project files
docker_build_files_location | /home/{{ acme_cert_owner }}/{{ docker_project_name }}" | Location for project files
#docker_volume_location | "/mnt/wsl/docker-desktop-data/version-pack-data/community/docker/volumes" # WSL2
docker_config_volume_name | "nginx_conf" | Directory name to mount docker volume with nginx config
docker_html_volume_name | "nginx_html" | Directory name to mount docker volume with website content
docker_config_volume_path | "{{ docker_build_files_location }}/{{ docker_config_volume_name }} | Directory to mount docker volume with nginx config
docker_html_volume_path | "{{ docker_build_files_location }}/{{ docker_html_volume_name }} | Directory to mount docker volume with website content
nginx_conf_domain | docker-2.test_domain.com | host configuration for NGINX
rm_containers | no | remove containers in the end (debug)
create_self_signed_cert | yes | Option to create self-signed certificate (required to run http-01 test)

acme_ssl_certificates:
  - { domain: 'docker-2.test_domain.com', challenge: 'http-01',   dns_zone: 'test_domain.com',     server: '12.34.56.78' }
```
## Dependencies
------------

collections:
  - 'ansible.posix'
  - 'community.general'
  - 'community.crypto'

## Example Playbook
----------------
`ansible-playbook RBT_playbook.yaml -i inventory.yaml`

```
---
- name: "Create Nginx container and issue Lets Encrypt certificates"
  hosts: "docker"
  gather_facts: false
  vars:
    docker_project_name: "dockerdir"
    docker_build_files_location: "/home/{{ acme_cert_owner }}/{{ docker_project_name }}"
    docker_config_volume_name: "nginx_conf"
    docker_html_volume_name: "nginx_html"
    docker_config_volume_path: "{{ docker_build_files_location }}/{{ docker_config_volume_name }}"
    docker_html_volume_path: "{{ docker_build_files_location }}/{{ docker_html_volume_name }}"
    acme_cert_storage_dir: "/home/{{ acme_cert_owner }}/letsencrypt_certs"
    acme_account_name: username
    nginx_conf_domain: name.test_domain.com
    acme_account_email: maxpanteleyev@gmail.com
    acme_directory: "https://acme-staging-v02.api.letsencrypt.org/directory"
    acme_cert_owner: test_domain
    rm_containers: no
    create_self_signed_cert: yes
    acme_ssl_certificates:
      - { domain: 'docker-2.test_domain.com', challenge: 'http-01',   dns_zone: 'test_domain.com',     server: '12.34.56.78' }
  tasks:
  - name: Generate directories to store project
    ansible.builtin.file:
      path: "{{ item }}"
      state: directory
      mode: 0755
    loop: 
      - "{{ docker_build_files_location }}"
      - "{{ docker_config_volume_path }}"
      - "{{ docker_html_volume_path }}"
      - "{{ docker_config_volume_path }}/ssl"

  - name: "Prepare compose"
    ansible.builtin.template:
      src: compose.yaml.j2
      dest: "{{ docker_build_files_location }}/compose.yaml"
      mode: '0644'
  
  - name: "Prepare nginx config"
    ansible.builtin.template:
      src: website.conf.j2
      dest: "{{ docker_config_volume_path }}/website.conf"
      mode: '0644'
  
  - name: "Copy website content"
    ansible.builtin.copy:
      src: "{{ item }}"
      dest: "{{ docker_html_volume_path }}/{{ item }}"
      mode: 0644
    loop:
      - "index.html"
      - "linux.png"

  - name: "Include rbt.letsencrypt"
    ansible.builtin.include_role:
      name: "rbt.letsencrypt"
      tasks_from: main.yml

  - name: "Cleanup"
    block: 
    - name: Tear down existing services
      community.docker.docker_compose:
        project_src: "{{ docker_build_files_location }}"
        project_name: "{{ docker_project_name }}"
        state: absent
  
    - name: "Tear down volumes"
      community.docker.docker_volume:
        name: "{{ item }}"
        state: absent
      loop: 
        - "{{ docker_project_name }}_{{ docker_config_volume_name }}" 
        - "{{ docker_project_name }}_{{ docker_html_volume_name }}" 
    when: rm_containers
```
