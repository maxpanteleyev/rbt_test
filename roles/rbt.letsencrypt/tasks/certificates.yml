---
# Generate CSR for LetsEncrypt (LE)
- name: "Generate directories to store"
  ansible.builtin.file:
    path: "{{ acme_cert_storage_dir }}/{{ acme_certificate.domain }}"
    state: directory
    mode: 0770
    owner: "{{ acme_cert_owner }}"
    group: "{{ acme_cert_owner }}"
  delegate_to: localhost

- name: "Generate directories to store"
  ansible.builtin.file:
    path: "{{ acme_cert_storage_dir }}/{{ acme_certificate.domain }}/{{ item }}"
    state: directory
    mode: 0770
  loop:
    - certs
    - requests
    - keys
  delegate_to: localhost

- block:
  - name: "Generate private key for {{ acme_certificate.domain }}"
    community.crypto.openssl_privatekey: 
      path: "{{ acme_cert_storage_dir }}/{{ acme_certificate.domain }}/keys/{{ acme_certificate.domain }}.key"
      size: "{{ acme_key_size }}"
      mode: 0660
      owner: "{{ acme_cert_owner }}"
      group: "{{ acme_cert_owner }}"
    delegate_to: localhost

  - block:
    - name: "Generate SAN for {{ acme_certificate.domain }}"
      set_fact:
        acme_certificate_san: "{{ acme_certificate.domain }},{{ acme_certificate.altname | join(',') }}"
      delegate_to: localhost

    - name: "Add SAN prefixes for {{ acme_certificate.domain }}"
      set_fact:
        acme_certificate_san: "{{ acme_certificate_san | split(',') | map('regex_replace', '^', 'DNS:') }}"
      delegate_to: localhost
  
    - name: "Generate new CSR+SAN for {{ acme_certificate.domain }}"
      community.crypto.openssl_csr:
        path: "{{ acme_cert_storage_dir }}/{{ acme_certificate.domain }}/requests/{{ acme_certificate.domain }}.csr"
        privatekey_path: "{{ acme_cert_storage_dir }}/{{ acme_certificate.domain }}/keys/{{ acme_certificate.domain }}.key"
        common_name: "{{ acme_certificate.domain }}"
        subject_alt_name: "{{ acme_certificate_san }}"
        mode: 0660
        owner: "{{ acme_cert_owner }}"
        group: "{{ acme_cert_owner }}"
      delegate_to: localhost
    when: acme_certificate.altname is defined

  - name: "Generate new CSR for {{ acme_certificate.domain }}"
    community.crypto.openssl_csr:
      path: "{{ acme_cert_storage_dir }}/{{ acme_certificate.domain }}/requests/{{ acme_certificate.domain }}.csr"
      privatekey_path: "{{ acme_cert_storage_dir }}/{{ acme_certificate.domain }}/keys/{{ acme_certificate.domain }}.key"
      common_name: "{{ acme_certificate.domain }}"
      subject_alt_name: "DNS:{{ acme_certificate.domain }}"
      mode: 0660
      owner: "{{ acme_cert_owner }}"
      group: "{{ acme_cert_owner }}"
    delegate_to: localhost
    when: acme_certificate.altname is not defined

# To run nginx for future LE challenge we have to generate self-signed certificate first:
  - block: 
    - name: "Generate self-signed certificate for {{ acme_certificate.domain }}"
      community.crypto.x509_certificate:
        path: "{{ acme_cert_storage_dir }}/{{ acme_certificate.domain }}/certs/{{ acme_certificate.domain }}.crt"
        privatekey_path: "{{ acme_cert_storage_dir }}/{{ acme_certificate.domain }}/keys/{{ acme_certificate.domain }}.key"
        csr_path: "{{ acme_cert_storage_dir }}/{{ acme_certificate.domain }}/requests/{{ acme_certificate.domain }}.csr"
        provider: selfsigned
        mode: 0660
        owner: "{{ acme_cert_owner }}"
        group: "{{ acme_cert_owner }}"
      delegate_to: localhost
    when: acme_certificate.challenge == 'http-01'
